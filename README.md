# Deleting a file through Symfony5 Console

## Abstract 
Deleting a file through Symfony5 Console

## Premise
This project requires :

* PHP 7.4
* Composer

They have to be up and running. 

## Installation
*  Download and install Symfony5 framework, via composer, on your local computer . For all info click [here](https://symfony.com) 
 *  The root folder of the project is 
 
       >**symfony_console_delete_file** 
    
 * and the absolute path is
 
      >**/var/www/html/symfony_console_delete_file**
    
  * Clone the project from gitlab on Desktop :
    
    >**git@gitlab.com:greengo_axe/symfony_console_delete_file.git**

* copy and overwrite all folders and files from 

>>**Desktop/symfony_console_delete_file**

>into 

>>**/var/www/html/symfony_console_delete_file**

* Delete the folder on Desktop (**Desktop/symfony_console_delete_file**)

* Enjoy!

## Troubleshooting
This project has been developed on Linux OS and there might be a few problems with the path  on Windows / Apple OSs.

* The direct command is :

>> **cd /var/www/html/symfony_console_delete_file**

>> php bin/console app:delete-file 'filename.ext'

>>for example

>>>** php bin/console app:delete-file 'text.txt' **