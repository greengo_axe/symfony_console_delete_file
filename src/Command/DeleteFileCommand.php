<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;


/**
 * Description of DeleteFileCommand
 */
class DeleteFileCommand extends Command{
    protected static $defaultName = 'app:delete-file';
    
    public function __construct()
       {
           parent::__construct();
    }
       
    protected function configure()
    {
          $this->setDescription('Delete file')
            ->setHelp('Delete file via CLI. Usefull for cronjob')
            ->addArgument('filename', InputArgument::REQUIRED, ' filename to delete.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->DeleteFile($input->getArgument('filename'));
        return Command::SUCCESS;
    }
    
    protected function DeleteFile($filename){
        $path_txt = getcwd().'/public/txt/';
        echo $path_txt.$filename;
        if (file_exists($path_txt.$filename)) {
            unlink($path_txt.$filename);
        } 
    }
}
